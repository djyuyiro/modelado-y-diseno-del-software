public class Automóvil {
    String marca;
    int modelo;
    double motor;
    enum TipoCombustible { GASOLINA, BIOETANOL, DIÉSEL, BIODIÉSEL, GAS_NATURAL }
    TipoCombustible tipoCombustible;
    enum TipoAutomóvil { CIUDAD, SUBCOMPACTO, COMPACTO, FAMILIAR, EJECUTIVO, SUV }
    TipoAutomóvil tipoAutomóvil;
    int númeroPuertas;
    int cantidadAsientos;
    int velocidadMáxima;
    enum TipoColor { BLANCO, NEGRO, ROJO, NARANJA, AMARILLO, VERDE, AZUL, VIOLETA }
    TipoColor color;
    int velocidadActual = 0;

    public Automóvil(String marca, int modelo, double motor, TipoCombustible tipoCombustible, TipoAutomóvil tipoAutomóvil, int númeroPuertas, int cantidadAsientos, int velocidadMáxima, TipoColor color) {
        this.marca = marca;
        this.modelo = modelo;
        this.motor = motor;
        this.tipoCombustible = tipoCombustible;
        this.tipoAutomóvil = tipoAutomóvil;
        this.númeroPuertas = númeroPuertas;
        this.cantidadAsientos = cantidadAsientos;
        this.velocidadMáxima = velocidadMáxima;
        this.color = color;
    }

    public String getMarca() {
        return marca;
    }

    public int getModelo() {
        return modelo;
    }

    public double getMotor() {
        return motor;
    }

    public TipoCombustible getTipoCombustible() {
        return tipoCombustible;
    }

    public TipoAutomóvil getTipoAutomóvil() {
        return tipoAutomóvil;
    }

    public int getNúmeroPuertas() {
        return númeroPuertas;
    }

    public int getCantidadAsientos() {
        return cantidadAsientos;
    }

    public int getVelocidadMáxima() {
        return velocidadMáxima;
    }

    public TipoColor getColor() {
        return color;
    }

    public int getVelocidadActual() {
        return velocidadActual;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public void setModelo(int modelo) {
        this.modelo = modelo;
    }

    public void setMotor(double motor) {
        this.motor = motor;
    }

    public void setTipoCombustible(TipoCombustible tipoCombustible) {
        this.tipoCombustible = tipoCombustible;
    }

    public void setTipoAutomóvil(TipoAutomóvil tipoAutomóvil) {
        this.tipoAutomóvil = tipoAutomóvil;
    }

    public void setNúmeroPuertas(int númeroPuertas) {
        this.númeroPuertas = númeroPuertas;
    }

    public void setCantidadAsientos(int cantidadAsientos) {
        this.cantidadAsientos = cantidadAsientos;
    }

    public void setVelocidadMáxima(int velocidadMáxima) {
        this.velocidadMáxima = velocidadMáxima;
    }

    public void setColor(TipoColor color) {
        this.color = color;
    }

    public void setVelocidadActual(int velocidadActual) {
        this.velocidadActual = velocidadActual;
    }

    public void acelerar(int incrementoVelocidad) {
        if (velocidadActual + incrementoVelocidad <= velocidadMáxima) {
            velocidadActual += incrementoVelocidad;
            System.out.println("Velocidad actual: " + velocidadActual + " km/h");
        } else {
            System.out.println("No se puede acelerar más allá de la velocidad máxima.");
        }
    }

    public void desacelerar(int decrementoVelocidad) {
        if (velocidadActual - decrementoVelocidad >= 0) {
            velocidadActual -= decrementoVelocidad;
            System.out.println("Velocidad actual: " + velocidadActual + " km/h");
        } else {
            System.out.println("No se puede desacelerar a una velocidad negativa.");
        }
    }

    public void frenar() {
        velocidadActual = 0;
        System.out.println("Velocidad actual: " + velocidadActual + " km/h");
    }

    public double calcularTiempoLlegada(int distancia) {
        if (velocidadActual > 0) {
            return (double) distancia / velocidadActual;
        } else {
            System.out.println("El automóvil está detenido. No se puede calcular el tiempo de llegada.");
            return -1; // Valor negativo para indicar un error
        }
    }

    public void imprimir() {
        System.out.println("Marca: " + marca);
        System.out.println("Modelo: " + modelo);
        System.out.println("Motor: " + motor + " litros");
        System.out.println("Tipo de combustible: " + tipoCombustible);
        System.out.println("Tipo de automóvil: " + tipoAutomóvil);
        System.out.println("Número de puertas: " + númeroPuertas);
        System.out.println("Cantidad de asientos: " + cantidadAsientos);
        System.out.println("Velocidad máxima: " + velocidadMáxima + " km/h");
        System.out.println("Color: " + color);
        System.out.println("Velocidad actual: " + velocidadActual + " km/h");
    }

    public static void main(String[] args) {
        Automóvil automóvil = new Automóvil("Ford", 2022, 2.0, TipoCombustible.GASOLINA, TipoAutomóvil.EJECUTIVO, 4, 5, 200, TipoColor.NEGRO);
        automóvil.imprimir();

        automóvil.setVelocidadActual(100);
        automóvil.acelerar(20);
        automóvil.desacelerar(50);
        automóvil.frenar();
    }
}