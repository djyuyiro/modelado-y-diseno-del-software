public class CuentaBancaria {
    String nombresTitular;
    String apellidosTitular;
    int numeroCuenta;
    enum Tipo { AHORROS, CORRIENTE }
    Tipo tipoCuenta;
    float saldo = 0;
    float interesMensual;

    CuentaBancaria(String nombresTitular, String apellidosTitular, int numeroCuenta, Tipo tipoCuenta, float interesMensual) {
        this.nombresTitular = nombresTitular;
        this.apellidosTitular = apellidosTitular;
        this.numeroCuenta = numeroCuenta;
        this.tipoCuenta = tipoCuenta;
        this.interesMensual = interesMensual;
    }

    void imprimir() {
        System.out.println("Nombres del titular = " + nombresTitular);
        System.out.println("Apellidos del titular = " + apellidosTitular);
        System.out.println("Número de cuenta = " + numeroCuenta);
        System.out.println("Tipo de cuenta = " + tipoCuenta);
        System.out.println("Saldo = " + saldo);
        System.out.println("Interés mensual = " + interesMensual);
    }

    void consultarSaldo() {
        System.out.println("El saldo actual es = " + saldo);
    }

    boolean consignar(int valor) {
        if (valor > 0) {
            saldo = saldo + valor;
            System.out.println("Se ha consignado $" + valor + " en la cuenta. El nuevo saldo es $" + saldo);
            return true;
        } else {
            System.out.println("El valor a consignar debe ser mayor que cero.");
            return false;
        }
    }

    boolean retirar(int valor) {
        if ((valor > 0) && (valor <= saldo)) {
            saldo = saldo - valor;
            System.out.println("Se ha retirado $" + valor + " en la cuenta. El nuevo saldo es $" + saldo);
            return true;
        } else {
            System.out.println("El valor a retirar debe ser menor que el saldo actual.");
            return false;
        }
    }

    void aplicarInteresMensual() {
        float interes = saldo * interesMensual;
        saldo = saldo + interes;
        System.out.println("Se ha aplicado el interés mensual. El nuevo saldo es $" + saldo);
    }

    void compararCuentas(CuentaBancaria cuenta) {
        if (saldo >= cuenta.saldo) {
            System.out.println("El saldo de la cuenta actual es mayor o igual al saldo de la cuenta pasada como parámetro.");
        } else {
            System.out.println("El saldo de la cuenta actual es menor al saldo de la cuenta pasada como parámetro.");
        }
    }

    void transferencia(CuentaBancaria cuenta, int valor) {
        if (retirar(valor)) {
            cuenta.consignar(valor);
        }
    }

    public static void main(String[] args) {
        CuentaBancaria cuenta1 = new CuentaBancaria("Pedro", "Pérez", 123456789, Tipo.AHORROS, 0.02f); // 2% de interés mensual
        CuentaBancaria cuenta2 = new CuentaBancaria("Pablo", "Pinzón", 44556677, Tipo.AHORROS, 0.02f); // 2% de interés mensual

        cuenta1.consignar(200000);
        cuenta2.consignar(100000);

        cuenta1.compararCuentas(cuenta2);
        cuenta1.transferencia(cuenta2, 50000);

        cuenta1.consultarSaldo();
        cuenta2.consultarSaldo();
    }
}
