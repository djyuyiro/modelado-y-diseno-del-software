public class Planeta {
    String nombre;
    int cantidadSatelites;
    double masa;
    double volumen;
    int diametro;
    int distanciaSol;
    double periodoOrbital; // Nuevo atributo: periodo orbital en años
    double periodoRotacion; // Nuevo atributo: periodo de rotación en días
    enum TipoPlaneta {GASEOSO, TERRESTRE, ENANO}
    TipoPlaneta tipo;
    boolean esObservable;

    Planeta(String nombre, int cantidadSatelites, double masa, double volumen, int diametro, int distanciaSol, double periodoOrbital, double periodoRotacion, TipoPlaneta tipo, boolean esObservable) {
        this.nombre = nombre;
        this.cantidadSatelites = cantidadSatelites;
        this.masa = masa;
        this.volumen = volumen;
        this.diametro = diametro;
        this.distanciaSol = distanciaSol;
        this.periodoOrbital = periodoOrbital; // Inicializar el nuevo atributo
        this.periodoRotacion = periodoRotacion; // Inicializar el nuevo atributo
        this.tipo = tipo;
        this.esObservable = esObservable;
    }

    void imprimir() {
        System.out.println("Nombre del planeta: " + nombre);
        System.out.println("Cantidad de satélites: " + cantidadSatelites);
        System.out.println("Masa (kg): " + masa);
        System.out.println("Volumen (km*3): " + volumen);
        System.out.println("Diámetro (km): " + diametro);
        System.out.println("Distancia al Sol (millones de km): " + distanciaSol);
        System.out.println("Tipo de planeta: " + tipo);
        System.out.println("Es observable a simple vista: " + esObservable);
        System.out.println("Periodo orbital (años): " + periodoOrbital); // Imprimir el nuevo atributo
        System.out.println("Periodo de rotación (días): " + periodoRotacion); // Imprimir el nuevo atributo
    }

    double calcularDensidad() {
        return masa / volumen;
    }

    boolean esPlanetaExterior() {
        double distanciaUA = distanciaSol / 149597870.0; // Convertir a UA
        return distanciaUA > 3.4 || distanciaUA < 2.1;
    }

    public static void main(String[] args) {
        Planeta planeta1 = new Planeta("Tierra", 1, 5.9736E24, 1.08321E12, 12742, 150, 1.0, 1.0, TipoPlaneta.TERRESTRE, true);
        Planeta planeta2 = new Planeta("Júpiter", 79, 1.899E27, 1.4313E15, 139820, 750, 11.9, 0.41, TipoPlaneta.GASEOSO, true);

        planeta1.imprimir();
        System.out.println("Densidad del planeta 1: " + planeta1.calcularDensidad());
        System.out.println("Es planeta exterior: " + planeta1.esPlanetaExterior());

        System.out.println();

        planeta2.imprimir();
        System.out.println("Densidad del planeta 2: " + planeta2.calcularDensidad());
        System.out.println("Es planeta exterior: " + planeta2.esPlanetaExterior());
    }
}
