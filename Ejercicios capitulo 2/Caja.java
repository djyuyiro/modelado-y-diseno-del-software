/**
 * Esta clase define objetos de tipo Caja con longitud, anchura, altura y tipo.
 */
public class Caja {
    double longitud;
    double anchura;
    double altura;
    String tipo;

    /**
     * Primer constructor que asigna valores a los atributos según los parámetros proporcionados.
     * @param longitud La longitud de la caja.
     * @param anchura La anchura de la caja.
     * @param altura La altura de la caja.
     */
    public Caja(double longitud, double anchura, double altura) {
        this.longitud = longitud;
        this.anchura = anchura;
        this.altura = altura;
        this.tipo = "Desconocido";
    }

    /**
     * Segundo constructor que inicializa todos los atributos con valores de cero.
     */
    public Caja() {
        this.longitud = 0;
        this.anchura = 0;
        this.altura = 0;
        this.tipo = "Desconocido";
    }

    /**
     * Tercer constructor que recibe un parámetro de longitud y asigna dicho valor a todos sus atributos.
     * @param longitud La longitud de la caja.
     */
    public Caja(double longitud) {
        this(longitud, longitud, longitud);
    }

    /**
     * Nuevo constructor que recibe como parámetros los valores de los cuatro atributos.
     * Este constructor invoca al primer constructor.
     * @param longitud La longitud de la caja.
     * @param anchura La anchura de la caja.
     * @param altura La altura de la caja.
     * @param tipo El tipo de la caja.
     */
    public Caja(double longitud, double anchura, double altura, String tipo) {
        this(longitud, anchura, altura);
        this.tipo = tipo;
    }
}