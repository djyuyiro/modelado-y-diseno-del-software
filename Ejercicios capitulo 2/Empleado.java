// Archivo: Empleado.java
public class Empleado {
    // Atributos
    private int identificador;
    private String nombre;
    private String apellidos;
    private int edad;

    // Constructores
    // Primer constructor sin parámetros
    public Empleado() {
        this.identificador = 100;
        this.nombre = "Nuevo empleado";
        this.apellidos = "Nuevo empleado";
        this.edad = 18;
    }

    // Segundo constructor con parámetros
    public Empleado(int identificador, String nombre, String apellidos, int edad) {
        this.identificador = identificador;
        this.nombre = nombre;
        this.apellidos = apellidos;
        this.edad = edad;
    }

    // Métodos adicionales si es necesario
}
