/**
 * Método main que define una cierta cantidad de metros y los
 * convierte a diferentes unidades de longitud, área y volumen.
 */
public class Main {
    public static void main(String args[]) {
        ConversorMetros conversorMetros = new ConversorMetros(3.5);
        System.out.println("Metros = " + conversorMetros.metros);
        System.out.println("Metros a centímetros = " + conversorMetros.convertirMetrosToCentimetros());
        System.out.println("Metros a milímetros = " + conversorMetros.convertirMetrosToMilimetros());
        System.out.println("Metros a pulgadas = " + conversorMetros.convertirMetrosToPulgadas());
        System.out.println("Metros a pies = " + conversorMetros.convertirMetrosToPies());
        System.out.println("Metros a yardas = " + conversorMetros.convertirMetrosToYardas());

        ConversorArea conversorArea = new ConversorArea(5000);
        System.out.println("\nÁrea = " + conversorArea.area);
        System.out.println("Área a centímetros cuadrados = " + conversorArea.convertirMetrosACentimetrosCuadrados());
        System.out.println("Área a hectáreas = " + conversorArea.convertirMetrosAHectareas());
        System.out.println("Área a kilómetros cuadrados = " + conversorArea.convertirMetrosAKilometrosCuadrados());
        System.out.println("Área a fanegas = " + conversorArea.convertirMetrosAFanegas());
        System.out.println("Área a acres = " + conversorArea.convertirMetrosAAcres());

        ConversorVolumen conversorVolumen = new ConversorVolumen(100);
        System.out.println("\nLitros = " + conversorVolumen.litros);
        System.out.println("Litros a galones = " + conversorVolumen.convertirLitrosAGalones());
        System.out.println("Litros a pintas = " + conversorVolumen.convertirLitrosAPintas());
        System.out.println("Litros a barriles = " + conversorVolumen.convertirLitrosABarriles());
        System.out.println("Litros a metros cúbicos = " + conversorVolumen.convertirLitrosAMetrosCubicos());
        System.out.println("Litros a hectolitros = " + conversorVolumen.convertirLitrosAHectolitros());
    }
}
