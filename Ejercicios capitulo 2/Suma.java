/**
 * Clase que realiza sumas sobrecargadas.
 * @version 1.0
 */
public class Suma {

    /**
     * Método que obtiene la suma de dos valores enteros.
     * @param a Primer valor entero.
     * @param b Segundo valor entero.
     * @return La suma de los dos valores enteros.
     */
    public int sumar(int a, int b) {
        return a + b;
    }

    /**
     * Método que obtiene la suma de tres valores enteros.
     * @param a Primer valor entero.
     * @param b Segundo valor entero.
     * @param c Tercer valor entero.
     * @return La suma de los tres valores enteros.
     */
    public int sumar(int a, int b, int c) {
        return a + b + c;
    }

    /**
     * Método que obtiene la suma de dos valores double.
     * @param a Primer valor double.
     * @param b Segundo valor double.
     * @return La suma de los dos valores double.
     */
    public double sumar(double a, double b) {
        return a + b;
    }

    /**
     * Método que obtiene la suma de tres valores double.
     * @param a Primer valor double.
     * @param b Segundo valor double.
     * @param c Tercer valor double.
     * @return La suma de los tres valores double.
     */
    public double sumar(double a, double b, double c) {
        return a + b + c;
    }

    /**
     * Método main para probar la clase Suma.
     * @param args Argumentos de la línea de comandos (no se utilizan).
     */
    public static void main(String[] args) {
        Suma suma = new Suma();

        // Prueba de los métodos sumar
        System.out.println("Suma de enteros (2 + 3): " + suma.sumar(2, 3));
        System.out.println("Suma de enteros (2 + 3 + 4): " + suma.sumar(2, 3, 4));
        System.out.println("Suma de double (2.5 + 3.5): " + suma.sumar(2.5, 3.5));
        System.out.println("Suma de double (2.5 + 3.5 + 4.5): " + suma.sumar(2.5, 3.5, 4.5));
    }
}
