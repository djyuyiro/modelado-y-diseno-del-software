public class Rombo {
    double diagonalMayor;
    double diagonalMenor;

    public Rombo(double diagonalMayor, double diagonalMenor) {
        this.diagonalMayor = diagonalMayor;
        this.diagonalMenor = diagonalMenor;
    }

    public double calcularArea() {
        return (diagonalMayor * diagonalMenor) / 2;
    }

    public double calcularPerímetro() {
        double lado = Math.sqrt((diagonalMayor * diagonalMayor + diagonalMenor * diagonalMenor) / 4);
        return 4 * lado;
    }
}
