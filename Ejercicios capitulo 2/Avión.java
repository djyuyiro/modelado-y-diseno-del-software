/**
 * Esta clase define objetos de tipo Avión con un fabricante y número de
 * motores como atributos.
 * @version 1.2/2020
 */
public class Avión {
    private String fabricante; /* Atributo que define el nombre del fabricante del avión */
    private int númeroMotores; /* Atributo que define el número de motores del avión */

    /**
     * Constructor de la clase Avión
     * @param fabricante      Parámetro que define el nombre del fabricante de un avión
     * @param númeroMotores   Parámetro que define el número de motores que tiene un avión
     */
    public Avión(String fabricante, int númeroMotores) {
        this.fabricante = fabricante;
        this.númeroMotores = númeroMotores;
    }

    /**
     * Método que devuelve el nombre del fabricante de un avión
     * @return El nombre del fabricante de un avión
     */
    public String getFabricante() {
        return fabricante;
    }

    /**
     * Método que establece el nombre de un fabricante de un avión
     * @param fabricante    Parámetro que define el nombre del fabricante de un avión
     */
    public void setFabricante(String fabricante) {
        this.fabricante = fabricante;
    }

    /**
     * Método que devuelve el número de motores de un avión
     * @return El número de motores de un avión
     */
    public int getNúmeroMotores() {
        return númeroMotores;
    }

    /**
     * Método que imprime en pantalla el fabricante de un avión
     */
    public void imprimirFabricante() {
        System.out.println("El fabricante del avión es: " + fabricante);
    }

    /**
     * Método main que crea dos aviones y modifica sus fabricantes
     */
    public static void main(String args[]) {
        // Crear dos objetos Avión
        Avión avión1 = new Avión("Airbus", 4);
        Avión avión2 = new Avión("Boeing", 4);

        // Asignar el primer objeto al segundo
        avión2 = avión1;

        // Mostrar los valores de los atributos de los dos objetos
        System.out.println("Valores del avión1:");
        avión1.imprimirFabricante();
        System.out.println("Número de motores: " + avión1.getNúmeroMotores());

        System.out.println("\nValores del avión2:");
        avión2.imprimirFabricante();
        System.out.println("Número de motores: " + avión2.getNúmeroMotores());

        // Asignar el valor "Stealth" al atributo fabricante del segundo objeto
        avión2.setFabricante("Stealth");

        // Imprimir en pantalla el valor del atributo fabricante del primer objeto
        System.out.println("\nDespués de asignar 'Stealth' al avión2, el avión1 tiene el fabricante:");
        avión1.imprimirFabricante();
    }
}
