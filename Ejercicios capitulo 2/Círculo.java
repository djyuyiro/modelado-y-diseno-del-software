public class Círculo {
    int radio;

    public Círculo(int radio) {
        this.radio = radio;
    }

    public double calcularArea() {
        return Math.PI * Math.pow(radio, 2);
    }

    public double calcularPerímetro() {
        return 2 * Math.PI * radio;
    }
}
