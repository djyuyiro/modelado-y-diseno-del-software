/**
 * Clase para convertir medidas de superficie o área.
 */
public class ConversorArea {
    double area;

    final double FACTOR_METROS_A_HECTAREAS = 0.0001;
    final double FACTOR_METROS_A_KILOMETROS_CUADRADOS = 1e-6;
    final double FACTOR_METROS_A_FANEGAS = 1 / 6460.0;
    final double FACTOR_METROS_A_ACRES = 0.000247105;

    public ConversorArea(double area) {
        this.area = area;
    }

    public double convertirMetrosACentimetrosCuadrados() {
        return area * 10000.0;
    }

    public double convertirMetrosAHectareas() {
        return area * FACTOR_METROS_A_HECTAREAS;
    }

    public double convertirMetrosAKilometrosCuadrados() {
        return area * FACTOR_METROS_A_KILOMETROS_CUADRADOS;
    }

    public double convertirMetrosAFanegas() {
        return area * FACTOR_METROS_A_FANEGAS;
    }

    public double convertirMetrosAAcres() {
        return area * FACTOR_METROS_A_ACRES;
    }
}