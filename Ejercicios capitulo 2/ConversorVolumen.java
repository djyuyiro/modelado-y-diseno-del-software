
/**
 * Clase para convertir medidas de volumen.
 */
public class ConversorVolumen {
    double litros;

    final double FACTOR_LITROS_A_GALONES = 0.264172;
    final double FACTOR_LITROS_A_PINTAS = 2.11338;
    final double FACTOR_LITROS_A_BARRILES = 0.00628981;
    final double FACTOR_LITROS_A_METROS_CUBICOS = 0.001;
    final double FACTOR_LITROS_A_HECTOLITROS = 0.01;

    public ConversorVolumen(double litros) {
        this.litros = litros;
    }

    public double convertirLitrosAGalones() {
        return litros * FACTOR_LITROS_A_GALONES;
    }

    public double convertirLitrosAPintas() {
        return litros * FACTOR_LITROS_A_PINTAS;
    }

    public double convertirLitrosABarriles() {
        return litros * FACTOR_LITROS_A_BARRILES;
    }

    public double convertirLitrosAMetrosCubicos() {
        return litros * FACTOR_LITROS_A_METROS_CUBICOS;
    }

    public double convertirLitrosAHectolitros() {
        return litros * FACTOR_LITROS_A_HECTOLITROS;
    }
}
