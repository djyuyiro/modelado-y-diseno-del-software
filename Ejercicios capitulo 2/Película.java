public class Película {
    private String nombre;
    private String director;
    private enum Tipo { ACCIÓN, COMEDIA, DRAMA, SUSPENSO }
    private Tipo género;
    private int duración;
    private int año;
    private double calificación;

    public Película(String nombre, String director, Tipo género, int duración, int año, double calificación) {
        this.nombre = nombre;
        this.director = director;
        this.género = género;
        this.duración = duración;
        this.año = año;
        this.calificación = calificación;
    }

    public String getNombre() {
        return nombre;
    }

    private void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDirector() {
        return director;
    }

    private void setDirector(String director) {
        this.director = director;
    }

    public Tipo getGénero() {
        return género;
    }

    private void setGénero(Tipo género) {
        this.género = género;
    }

    public int getDuración() {
        return duración;
    }

    private void setDuración(int duración) {
        this.duración = duración;
    }

    public int getAño() {
        return año;
    }

    private void setAño(int año) {
        this.año = año;
    }

    public double getCalificación() {
        return calificación;
    }

    private void setCalificación(double calificación) {
        this.calificación = calificación;
    }

    private void imprimirCartel() {
        System.out.println("-------- " + nombre + " ---------");
        System.out.println("Año: " + año);

        System.out.print("Género: ");
        for (int i = 0; i < género.ordinal() + 1; i++) {
            System.out.print(género.values()[i]);
            if (i < género.ordinal()) {
                System.out.print(", ");
            }
        }
        System.out.println();

        System.out.println("Director: " + director);

        int cantidadAsteriscos = (int) (calificación / 2);
        System.out.print("Valoración: ");
        for (int i = 0; i < cantidadAsteriscos; i++) {
            System.out.print("*,");
        }
        System.out.println();
    }

    private boolean esPelículaEpica() {
        return duración >= 180;
    }

    private String calcularValoración() {
        if (calificación >= 0 && calificación <= 2) {
            return "Muy mala";
        } else if (calificación > 2 && calificación <= 5) {
            return "Mala";
        } else if (calificación > 5 && calificación <= 7) {
            return "Regular";
        } else if (calificación > 7 && calificación <= 8) {
            return "Buena";
        } else if (calificación > 8 && calificación <= 10) {
            return "Excelente";
        } else {
            return "No tiene asignada una valoración válida";
        }
    }

    private boolean esSimilar(Película película) {
        return película.género == género && película.calcularValoración().equals(calcularValoración());
    }

    public static void main(String[] args) {
        Película película1 = new Película("Gandhi", "Richard Attenborough", Tipo.DRAMA, 191, 1982, 8.3);
        Película película2 = new Película("Thor", "Kenneth Branagh", Tipo.ACCIÓN, 115, 2011, 7.0);

        película1.imprimirCartel();
        System.out.println();
        película2.imprimirCartel();

        System.out.println("La película " + película1.getNombre() + " es épica: " + película1.esPelículaEpica());
        System.out.println("La película " + película2.getNombre() + " es épica: " + película2.esPelículaEpica());

        System.out.println("La película " + película1.getNombre() + " y la película " + película2.getNombre() + " son similares = " +
                película1.esSimilar(película2));
    }
}
