public class Rectángulo {
    int base;
    int altura;

    public Rectángulo(int base, int altura) {
        this.base = base;
        this.altura = altura;
    }

    public double calcularArea() {
        return base * altura;
    }

    public double calcularPerímetro() {
        return (2 * base) + (2 * altura);
    }
}
