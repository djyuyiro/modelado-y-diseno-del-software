/**
 * Clase para convertir medidas de longitud.
 */
public class ConversorMetros {
    double metros;

    final int FACTOR_METROS_CM = 100;
    final int FACTOR_METROS_MILIM = 1000;
    final double FACTOR_METROS_PULGADAS = 39.37;
    final double FACTOR_METROS_PIES = 3.28;
    final double FACTOR_METROS_YARDAS = 1.09361;

    public ConversorMetros(double metros) {
        this.metros = metros;
    }

    public double convertirMetrosToCentimetros() {
        return FACTOR_METROS_CM * metros;
    }

    public double convertirMetrosToMilimetros() {
        return FACTOR_METROS_MILIM * metros;
    }

    public double convertirMetrosToPulgadas() {
        return FACTOR_METROS_PULGADAS * metros;
    }

    public double convertirMetrosToPies() {
        return FACTOR_METROS_PIES * metros;
    }

    public double convertirMetrosToYardas() {
        return FACTOR_METROS_YARDAS * metros;
    }
}



